import React from "react";
import { connect } from "react-redux";
import { fetchListDetail } from "../../state/actions/get-list-details-action";
import FlipMove from "react-flip-move";
import { postListDetailData } from "../../state/actions/put-list-details-action";
import "./ListDetail.scss";

var urlParams = window.location.href;
var _id = urlParams.split("/")[4];
var shopItems = [];

class ListDetail extends React.Component {
  updateName(value) {
    this.setState({ name: value });
  }

  componentDidMount() {
    this.props.onFetchListDetails(_id);
  }

  render() {
    return (
      <main className="container-fluid main" id="main">
        <div className="container">
          <div className="row">
            <div className="col">
              <p>
                {this.props.shoppingListsDetails &&
                  this.props.shoppingListsDetails.title}
              </p>
              <ul id="shoplist">
                {this.props.shoppingListsDetails &&
                  this.props.shoppingListsDetails.items &&
                  this.props.shoppingListsDetails.items.map(
                    (element, index) => {
                      return <li key={index}>{element}</li>;
                    }
                  )}
              </ul>
            </div>
            <div className="ShoppingListMain">
              <div className="header">
                <input
                  id="inputShopping"
                  onKeyUp={e => this.updateName(e.target.value)}
                />
                <button
                  onClick={() => {
                    {
                      this.props.shoppingListsDetails &&
                        this.props.shoppingListsDetails.items &&
                        this.props.shoppingListsDetails.items.forEach(
                          element => {
                            shopItems.push(element);
                          }
                        );
                    }
                    this.state.name !== "" && shopItems.push(this.state.name);
                    this.props.onAddListDetail(_id, shopItems);

                    // this.props.onFetchListDetails(_id);
                  }}
                >
                  Add
                </button>
              </div>
              <div>
                <ul className="theList">
                  <FlipMove duration={250} easing="ease-out">
                    {/* {this.props.ShoppingList.map((shoppingElement, index) => (
                      <li
                        id={shoppingElement.id}
                        key={index}
                        onClick={() => {
                          this.props.onRemoveItem(shoppingElement.id);
                        }}
                      >
                        {shoppingElement.name}
                      </li>
                    ))} */}
                  </FlipMove>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </main>
    );
  }
}

const mapStateToProps = state => ({
  shoppingListsDetails: state.dataListDetail.lists
});

const mapDispatchToProps = dispatch => {
  return {
    onFetchListDetails: id => dispatch(fetchListDetail(id)),
    onAddListDetail: (id, lists) => dispatch(postListDetailData(id, lists))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListDetail);
