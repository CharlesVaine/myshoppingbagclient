import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Header from "../../components/Header";
import Home from "../Home";
import Lists from "../Lists";
import ListDetail from "../ListDetail";
import 'jquery/dist/jquery';
import 'bootstrap/dist/js/bootstrap.bundle.js';
import 'bootstrap/dist/css/bootstrap.css';
import './App.scss';


class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/lists" component={Lists} />
          <Route exact path="/lists/:listId" component={ListDetail} />
          {/* <Route component={NotFound} /> */}
        </Switch>
      </BrowserRouter>
    )
  }
}

export default App;