import React from "react";
import { connect } from "react-redux";
import { Link, NavLink } from "react-router-dom";
import { fetchListData } from "../../state/actions/get-lists-action";
import { postListData } from "../../state/actions/post-lists-action";
import FlipMove from "react-flip-move";
import { EyeIcon, PlusIconLg } from "./../../assets/Icons/Icons";
import "./Lists.scss";

class Lists extends React.Component {
  state = {
    title: ""
  };

  updateTitle(value) {
    this.setState({ title: value });
  }

  componentDidMount() {
    this.props.onFetchList();
  }
  render() {
    return (
      <React.Fragment>
        <main className="container-fluid main" id="main">
          <div className="container">
            {/* <div>{this.props.shoppingList && this.props.shoppingList}</div> */}
            <div className="row">
              {this.props.shoppingList &&
                this.props.shoppingList.map((list, index) => {
                  return (
                    <div
                      key={index}
                      id={list._id}
                      className="col-12 col-md-6 col-lg-4"
                    >
                      <div className="card border-info shadow">
                        <div className="card-header text-light bg-info">
                          {list.title}
                        </div>
                        <div className="card-body">
                          {list.items.map((item, index) => {
                            return (
                              <div
                                key={index}
                                className="list-item text-secondary border-bottom p-1"
                              >
                                {item}
                              </div>
                            );
                          })}
                        </div>
                        <div className="card-footer text-info bg-light">
                          <Link
                            className="btn btn-info btn-sm"
                            to={{ pathname: `/lists/${list._id}` }}
                          >
                            <EyeIcon />
                          </Link>
                        </div>
                      </div>
                    </div>
                  );
                })}
              <div className="col-12 col-md-6 col-lg-4">
                <div className="card shadow border-info add-list">
                  <div className="card-body">
                    <button
                      type="button"
                      className="btn btn-default text-info"
                      data-toggle="modal"
                      data-target="#AddModal"
                    >
                      <PlusIconLg />
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>

        <div
          className="modal fade"
          id="AddModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="AddModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title text-info" id="AddModalLabel">
                  Nueva Lista
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form>
                  <div className="form-group">
                    <label htmlFor="listName">Nombre de la lista</label>
                    <input
                      type="text"
                      className="form-control"
                      id="listName"
                      placeholder="Lista"
                      onKeyUp={e => this.updateTitle(e.target.value)}
                    />
                  </div>
                </form>
              </div>
              {/* <div className="ShoppingListMain">
                <div className="header">
                  <input
                    id="inputShopping"
                    onKeyUp={e => this.updateName(e.target.value)}
                  />
                  <button
                    onClick={() => {
                      this.state.name !== "" &&
                        this.props.onAddItem(
                          Math.floor(Math.random() * 99) + 1,
                          this.state.name
                        );
                    }}
                  >
                    Add
                  </button>
                  {!this.props.isFetching && <p style={{ color: "gold" }} />}
                  {this.props.isFetching && (
                    <p style={{ color: "gold" }}>Fetching new item...</p>
                  )}
                </div>
                <div>
                  <ul className="theList">
                    <FlipMove duration={250} easing="ease-out">
                      {this.props.ShoppingList.map((shoppingElement, index) => (
                        <li
                          id={shoppingElement.id}
                          key={index}
                          onClick={() => {
                            this.props.onRemoveItem(shoppingElement.id);
                          }}
                        >
                          {shoppingElement.name}
                        </li>
                      ))}
                    </FlipMove>
                  </ul>
                </div>
              </div> */}
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-danger"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button
                  type="button"
                  className="btn btn-info"
                  onClick={() => {
                    this.state.title !== "" &&
                      this.props.onAddList(this.state.title);
                  }}
                >
                  Save changes
                </button>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  shoppingList: state.dataList.lists
});

const mapDispatchToProps = dispatch => {
  return {
    onFetchList: () => dispatch(fetchListData()),
    onAddList: lists => dispatch(postListData(lists))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Lists);
