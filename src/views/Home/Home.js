import React from "react";

class Home extends React.Component {
  render() {
    return (
      <main className="container-fluid main" id="main">
        <div className="container">
          <div className="row">
            <div className="col">
              Home
            </div>
          </div>
        </div>
      </main>
    )
  }
}

export default Home;