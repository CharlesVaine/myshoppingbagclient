import React from "react";
import { Link, NavLink } from "react-router-dom";
import logo from '../../logo.svg';
import "./Header.scss";

class Header extends React.Component {
  render() {
    return (
      <header className="container-fluid fixed-top" id="header">
        <nav className="navbar navbar-expand-md navbar-dark bg-dark">
          <NavLink className="navbar-brand" to="/" exact>
            <img src={logo} className="logo" alt="logo" />
          </NavLink>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <NavLink variant="button" activeClassName="active" className="nav-link" to="/" exact>Home</NavLink>
              </li>
              <li className="nav-item">
                <NavLink variant="button" activeClassName="active" className="nav-link" to="/lists">Lists</NavLink>
              </li>
              {/* <li className="nav-item">
                <NavLink variant="button" activeClassName="active" className="nav-link" to="/list/:id">List Detail</NavLink>
              </li> */}
            </ul>
          </div>
        </nav>
      </header>
    )
  }
}

export default Header;