export const GET_ACTION = "GET_ACTION";
export const getAction = () => {
  return {
    type: GET_ACTION
  };
};

export const GET_ACTION_SUCCESS = "GET_ACTION_SUCCESS";
export const getActionSuccess = lists => {
  return {
    type: GET_ACTION_SUCCESS,
    lists
  };
};

export const GET_ACTION_FAILURE = "GET_ACTION_FAILURE";
export const getActionFailure = () => {
  return {
    type: GET_ACTION_FAILURE
  };
};

// THUNK

export const fetchListData = () => {
  // return function
  return dispatch => {
    dispatch(getAction());
    var miConfig = {
      method: "GET",
      headers: { "content-Type": "application/json" },
      mode: "cors",
      cache: "default"
    };
    return fetch(`http://localhost:3000/list`, miConfig)
      .then(
        response => response.json(),
        error => console.log("An error occurred", error)
      )
      .then(json => {
        dispatch(getActionSuccess(json));
      })
      .catch(function(error) {
        console.log("Hubo un problema con la petición Fetch:" + error.message);
        dispatch(getActionFailure());
      });
  };
};
