// ACTIONS
export const PUT_ACTION = "PUT_ACTION";
export const PutAction = () => ({
  type: PUT_ACTION
});

export const PUT_ACTION_SUCCESS = "PUT_ACTION_SUCCESS";
export const PutActionSuccess = lists => ({
  type: PUT_ACTION_SUCCESS,
  lists
});

export const PUT_ACTION_FAILURE = "PUT_ACTION_FAILURE";
export const PutActionFailure = () => ({
  type: PUT_ACTION_FAILURE
});

// THUNK FUNCTION

export const postListDetailData = (id, items) => {
  return dispatch => {
    dispatch(PutAction());

    return fetch(`http://localhost:3000/list/${id}`, {
      method: "PUT", // or 'PUT'
      body: JSON.stringify({ items }), // data can be `string` or {object}!
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred", error)
      )
      .then(() => {
        dispatch(PutActionSuccess({ items }));
      })

      .catch(function(error) {
        console.log("Hubo un problema con la petición Fetch:" + error.message);
        dispatch(PutActionFailure());
      });
  };
};
