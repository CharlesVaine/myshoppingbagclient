// ACTIONS
export const POST_ACTION = "POST_ACTION";
export const postAction = () => ({
  type: POST_ACTION
});

export const POST_ACTION_SUCCESS = "POST_ACTION_SUCCESS";
export const postActionSuccess = title => ({
  type: POST_ACTION_SUCCESS,
  lists: {
    title
  }
});

export const POST_ACTION_FAILURE = "POST_ACTION_FAILURE";
export const postActionFailure = () => ({
  type: POST_ACTION_FAILURE
});

// THUNK FUNCTION

export const postListData = title => {
  return dispatch => {
    dispatch(postAction());

    return fetch("http://localhost:3000/list?", {
      method: "POST", // or 'PUT'
      body: JSON.stringify({ title }), // data can be `string` or {object}!
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred", error)
      )
      .then(() => {
        dispatch(postActionSuccess(title));
      })

      .catch(function(error) {
        console.log("Hubo un problema con la petición Fetch:" + error.message);
        dispatch(postActionFailure());
      });
  };
};
