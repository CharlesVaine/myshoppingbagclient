export const GET_DETAIL_ACTION = "GET_DETAIL_ACTION";
export const getDetailAction = () => {
  return {
    type: GET_DETAIL_ACTION
  };
};

export const GET_DETAIL_ACTION_SUCCESS = "GET_DETAIL_ACTION_SUCCESS";
export const getDetailActionSuccess = lists => {
  return {
    type: GET_DETAIL_ACTION_SUCCESS,
    lists
  };
};

export const GET_DETAIL_ACTION_FAILURE = "GET_DETAIL_ACTION_FAILURE";
export const getDetailActionFailure = () => {
  return {
    type: GET_DETAIL_ACTION_FAILURE
  };
};

// THUNK

export const fetchListDetail = id => {
  // return function
  return dispatch => {
    dispatch(getDetailAction(id));
    var miConfig = {
      method: "GET",
      headers: { "content-Type": "application/json" },
      mode: "cors",
      cache: "default"
    };
    return fetch(`http://localhost:3000/list/${id}`, miConfig)
      .then(
        response => response.json(),
        error => console.log("An error occurred", error)
      )
      .then(json => {
        dispatch(getDetailActionSuccess(json));
      })
      .catch(function(error) {
        console.log("Hubo un problema con la petición Fetch:" + error.message);
        dispatch(getDetailActionFailure());
      });
  };
};
