import {
  GET_DETAIL_ACTION,
  GET_DETAIL_ACTION_FAILURE,
  GET_DETAIL_ACTION_SUCCESS
} from "../actions/get-list-details-action";

const initialState = {
  lists: [],
  isFetching: false,
  error: false
};

function GetListsDetailReducer(prevState = initialState, action) {
  switch (action.type) {
    case GET_DETAIL_ACTION:
      return {
        ...prevState,
        lists: [],
        isFetching: true
      };
    case GET_DETAIL_ACTION_SUCCESS:
      return {
        ...prevState,
        lists: action.lists,
        isFetching: false
      };
    case GET_DETAIL_ACTION_FAILURE:
      return {
        ...prevState,
        isFetching: false,
        error: true
      };
    default:
      return {
        prevState
      };
  }
}

export default GetListsDetailReducer;
