import {
  PUT_ACTION,
  PUT_ACTION_SUCCESS,
  PUT_ACTION_FAILURE
} from "../actions/put-list-details-action";

const initialState = {
  lists: [
    {
      items: []
    }
  ],
  isFetching: false,
  error: false
};

function PutListsReducer(prevState = initialState, action) {
  switch (action.type) {
    case PUT_ACTION:
      return {
        ...prevState,
        lists: [],
        isFetching: true
      };
    case PUT_ACTION_SUCCESS:
      return {
        ...prevState,
        lists: [...prevState.lists, action.lists],
        isFetching: false
      };

    case PUT_ACTION_FAILURE:
      return {
        ...prevState,
        isFetching: false,
        error: true
      };
    default:
      return prevState;
  }
}

export default PutListsReducer;
