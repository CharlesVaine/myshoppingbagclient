import {
  GET_ACTION,
  GET_ACTION_SUCCESS,
  GET_ACTION_FAILURE
} from "../actions/get-lists-action";

const initialState = {
  lists: [
    {
      title: "",
      items: []
    }
  ],
  isFetching: false,
  error: false
};

function GetListsReducer(prevState = initialState, action) {
  switch (action.type) {
    case GET_ACTION:
      return {
        ...prevState,
        lists: [],
        isFetching: true
      };
    case GET_ACTION_SUCCESS:
      return {
        ...prevState,
        lists: action.lists,
        isFetching: false
      };
    case GET_ACTION_FAILURE:
      return {
        ...prevState,
        isFetching: false,
        error: true
      };
    default:
      return {
        prevState
      };
  }
}

export default GetListsReducer;
