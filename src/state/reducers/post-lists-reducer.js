import {
  POST_ACTION,
  POST_ACTION_SUCCESS,
  POST_ACTION_FAILURE
} from "../actions/post-lists-action";

const initialState = {
  lists: [
    {
      title: ""
    }
  ],
  isFetching: false,
  error: false
};

function PostListsReducer(prevState = initialState, action) {
  switch (action.type) {
    case POST_ACTION:
      return {
        ...prevState,
        lists: [],
        isFetching: true
      };
    case POST_ACTION_SUCCESS:
      return {
        ...prevState,
        lists: action.lists,
        isFetching: false
      };
    case POST_ACTION_FAILURE:
      return {
        ...prevState,
        isFetching: false,
        error: true
      };
    default:
      return prevState;
  }
}

export default PostListsReducer;
