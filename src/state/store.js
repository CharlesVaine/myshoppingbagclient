import { combineReducers, createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import logger from "redux-logger";
import GetListsReducer from "./reducers/get-lists-reducer";
import PostListsReducer from "./reducers/post-lists-reducer";
import GetListsDetailReducer from "./reducers/get-list-details-reducer";
import PutListsReducer from "./reducers/put-list-details-reducer";

const reducers = combineReducers({
  dataList: GetListsReducer,
  dataListAdd: PostListsReducer,
  dataListDetail: GetListsDetailReducer,
  dataListPutDetail: PutListsReducer
});

const middlewares = applyMiddleware(thunk, logger);
const composeEnhancers = composeWithDevTools(middlewares);

const store = createStore(reducers, composeEnhancers);

export default store;
