import React from "react";
import { Icon } from "react-icons-kit";
import { eye } from "react-icons-kit/fa";
import { plus } from "react-icons-kit/fa";

export const EyeIcon = () => <Icon icon={eye} />
export const PlusIcon = () => <Icon icon={plus} />
export const PlusIconLg = () => <Icon icon={plus} size={50} />